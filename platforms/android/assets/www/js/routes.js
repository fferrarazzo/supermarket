angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
   .state('superMarketConfig', {
       url: '/page0',
       templateUrl: 'templates/superMarketConfig.html',
       controller: 'superMarketConfigCtrl',
       controllerAs: 'superMarketConfigCtrl'
     })

      .state('superMarket', {
    url: '/page1',
    templateUrl: 'templates/superMarket.html',
    controller: 'superMarketCtrl',
    controllerAs: 'superMarketCtrl',
    params: {ipServer:null}
  })

  .state('agregarSuperMercado', {
    url: '/page2',
    templateUrl: 'templates/agregarSuperMercado.html',
    controller: 'agregarSuperMercadoCtrl',
    controllerAs: 'agregarSuperMercadoCtrl',
    params: {ipServer:null}
  })

  .state('seleccioneSuperMercado', {
    url: '/page3',
    templateUrl: 'templates/seleccioneSuperMercado.html',
    controller: 'seleccioneSuperMercadoCtrl',
    controllerAs: 'seleccioneSuperMercadoCtrl',
    params: {ipServer:null}
  })

  .state('consulteProducto', {
    url: '/page4',
    templateUrl: 'templates/consulteProducto.html',
    controller: 'consulteProductoCtrl',
    controllerAs: 'consulteProductoCtrl',
    params: {ipServer:null,id_supermercados:null,nombreSuper:null,localidad:null}
  })

  .state('producto', {
    url: '/page5',
    templateUrl: 'templates/producto.html',
    controller: 'productoCtrl',
    controllerAs: 'productoCtrl',
    params: {ipServer:null,id_supermercados:null,nombre:null,foto:null,desc:null,unidad:null,id_producto:null}
  })

  .state('agregarProducto', {
    url: '/page6',
    templateUrl: 'templates/agregarProducto.html',
    controller: 'agregarProductoCtrl',
    controllerAs: 'agregarProductoCtrl',
    params: {ipServer:null,id_supermercados:null,id_producto:null,precio:null}
  })

$urlRouterProvider.otherwise('/page0')

  

});