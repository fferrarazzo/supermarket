// Ejemplo de un servicio REST accediendo a MySQL

var express=require('express');
var app = express();

var mysql      = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  port     : 3306,
  user     : 'root',
  password : 'Xj14p3r9',
  database : 'mydb'
});
 
connection.connect();


// Configuro respuesta para permitir accesos CORS
app.use(function(req,res,next) {
	res.header("Access-Control-Allow-Origin","*");
	res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
	next(); 
});

app.get('/provincias',function(req,res) {
        console.log("GET /provincias");
        connection.query('SELECT * from provincias', function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.get('/localidades',function(req,res) {
        console.log("GET /localidades");
        connection.query('SELECT * from localidades', function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.get('/supermercados',function(req,res) {
	console.log("GET /supermercados");
	connection.query('SELECT * from supermercados', function(err, rowsp, fields) {
		res.jsonp(rowsp)
	});
});

app.get('/supermercados/:id',function(req,res) {
        console.log("GET /supermercados/:id");
	var consulta = `SELECT * from supermercados WHERE id_supermercados = ${req.params.id}`;
        connection.query(consulta, function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.get('/productosasocsiados/:ids/:idp',function(req,res) {
        console.log("GET /productosasociados/:ids/:idp");
        var consulta = `SELECT s.nombre, p.precio FROM supermercados AS s JOIN prod_super AS p ON s.id_supermercados=p.id_supermercados WHERE s.id_localidad = (SELECT id_localidad FROM supermercados WHERE id_supermercados = ${req.params.ids}) AND p.id_productos = ${req.params.idp} ORDER BY RAND();`;
        connection.query(consulta, function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});
//INSERTS
app.get('/supermercados/:nombre/:direccion/:id_localidad/:id_provincia',function(req,res) {
        console.log("GET /supermercados/:nombre/:direccion");
        var consulta = 'INSERT INTO supermercados SET ?';
        connection.query(consulta, {nombre: req.params.nombre, direccion: req.params.direccion, id_localidad: req.params.id_localidad, id_provincia: req.params.id_provincia}
        , function(err, result) {
		if (err) throw err;
                res.jsonp(result.insertId)
        });
});

app.get('/producto/agregar/:id_productos/:descripcion/:imagen/:unidad/:cantidad',function(req,res) {
        console.log("GET /producto/agregar/:id_productos/:descripcion/:imagen/:unidad/:cantidad");
        var consulta = 'INSERT INTO productos SET ?';

        connection.query(consulta, {id_productos: req.params.id_productos, descripcion: req.params.descripcion, imagen: req.params.imagen, unidad: req.params.unidad, cantidad: req.params.cantidad}
        , function(err, result) {
		if (err) throw err;
                res.jsonp(result.insertId)
        });
});

app.get('/producto/prodSuper/:precio/:id_productos/:id_supermercados',function(req,res) {
        console.log("GET /producto/prodSuper/:precio/:id_productos/:id_supermercados");
        var consulta = 'INSERT INTO prod_super SET ?';
        connection.query(consulta, {precio: req.params.precio,id_productos: req.params.id_productos,id_supermercados: req.params.id_supermercados}
        , function(err, result) {
		if (err) throw err;
                res.jsonp(result.insertId)
        });
});

app.get('/productos',function(req,res) {
        console.log("GET /productos");
        connection.query('SELECT * from productos', function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.get('/producto/:id_producto/:id_supermercados',function(req,res) {
        var consulta = `SELECT p.id_productos, p.descripcion, p.imagen, p.unidad,p.cantidad
            FROM productos p inner join prod_super ps on p.id_productos = ps.id_productos
            WHERE p.id_productos = ${req.params.id_producto} and ps.id_supermercados = ${req.params.id_supermercados}`;
        console.log(consulta);
        connection.query(consulta,{id_productos: req.params.id_producto, id_supermercados : req.params.id_supermercados}, function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.get('/producto/:id_producto',function(req,res) {
        var consulta = `SELECT *
            FROM productos
            WHERE id_productos = ${req.params.id_producto}`;
        console.log(consulta);
        connection.query(consulta,{id_productos: req.params.id_producto}, function(err, rowsp, fields) {
                res.jsonp(rowsp)
        });
});

app.post('/foto/:nombre',function(req,res) {
	console.log("POST /foto/"+req.params.nombre);
	binaryData = new Buffer(req.body,'base64');
	fs.writeFile('./public/foto/'+req.params.nombre+'.jpg',binaryData);
});

app.listen(8000);
console.log("REST POS instalado en port 8000");
