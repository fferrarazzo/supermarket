angular.module('app.controllers', ['app.config','ionic.native'])

.controller('superMarketConfigCtrl', ['$scope', '$stateParams','Config','$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,Config,$state) {
	console.log("superMarketConfigCtrl");
    var self = this;
    self.submitIpServer = function(){
                Config.ENV.SERVER = self.ipServer;
                $state.go('superMarket',
                        {
                            server: self.ipServer,
                        })
                }
}])

.controller('superMarketCtrl', ['$scope', '$stateParams', 'Config','$state',  // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, Config,$state) {
	console.log("superMarketCtrl");
    $scope.ENV = $stateParams.ipServer;
    this.selSuperMercado = function(data, status, headers, Config){
                  $state.go('seleccioneSuperMercado',
                          {
                              ipServer: $stateParams.ipServer,
                          })
                  }
    this.addSuperMercado = function(data, status, headers, Config){
                  $state.go('agregarSuperMercado',
                          {
                              ipServer: $stateParams.ipServer,
                          })
                  }
}])

.controller('agregarSuperMercadoCtrl', ['$scope','$http', '$stateParams','Config', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope,$http, $stateParams,Config, $state ) {
	console.log("agregarSuperMercadoCtrl");
    var self = this;
            self.id;
            self.ipServer = $stateParams.ipServer;
    		self.nombreSuper;
    		self.direccionSuper;
    		self.selectedValueLocalidadSuper;
    		self.selectedValueProvinciaSuper;
    $http.get('http://'+Config.ENV.SERVER+':8000/provincias')
            .success(function(data, status, headers, config) {
            self.provinciasList = data;
            });
    $http.get('http://'+Config.ENV.SERVER+':8000/localidades')
            .success(function(data, status, headers, config) {
            self.localidadesList = data;
            });
    self.submit = function(){
    $http.get('http://'+Config.ENV.SERVER+':8000/supermercados/'+self.nombreSuper+'/'+self.direccionSuper+'/'+self.localidadSuperSelectedValue.id_localidad+'/'+self.provinciaSuperSelectedValue.id_provincia)
    .success(function(data, status, headers, Config) {
             		$state.go('superMarket',{ipServer: $stateParams.ipServer,})
                 });
    }
}])

.controller('seleccioneSuperMercadoCtrl', ['$scope', '$http', '$stateParams','Config', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $http, $stateParams,Config, $state ) {
	console.log("seleccioneSuperMercadoCtrl");
    var self = this;
    self.selectedValue;
    console.log(self.supers);
$http.get('http://'+Config.ENV.SERVER+':8000/supermercados')
        .success(function(data, status, headers, config) {
        console.log(data);
        self.marketList = data;
        });
    self.submit = function(){
        Config.ENV.SUPER_ID=self.selectedValue.id_supermercados;
        Config.ENV.SUPER_LOCALIDAD=self.selectedValue.localidad;
        console.log(Config.ENV.SERVER);
    			$state.go('consulteProducto',
    				{
    				    ipServer: Config.ENV.SERVER,
    				    id_supermercados: self.selectedValue.id_supermercados,
    				});
    	}
}])

.controller('consulteProductoCtrl', ['$scope', '$http', '$stateParams', '$cordovaBarcodeScanner', '$cordovaCamera', 'Config', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $http, $stateParams, $cordovaBarcodeScanner, $cordovaCamera, Config, $state) {
	console.log("consulteProductoCtrl");
	var self = this;
    self.precioCarga;
    self.idSuper = $stateParams.id_supermercados;
    self.ipServer = $stateParams.ipServer;
    console.log( $stateParams.ipServer);
    console.log( $stateParams.id_supermercados);
    this.submitBarCode = function(){
	console.log("Se llamó a la función scope.submitBarCode");
	$cordovaBarcodeScanner
      		.scan()
      		.then(function(barcodeData) {
			console.log("ID del producto = " + barcodeData.text);
			//Existe el Producto?
            $http.get('http://'+self.ipServer+':8000/producto/'+barcodeData.text+'/'+self.idSuper)
                    .success(function(data, status, headers, config) {
                    console.log(data);
                        if(data.length != 0){
				console.log("data.leght != 0");
                        //NAVEGO A LA PAGINA DE PRODUCTO
                        $state.go('producto',
                                {
                                    ipServer: self.ipServer,
                                    id_supermercados: self.idSuper,
                                    id_producto:barcodeData.text,
                                    nombre:data[0].descripcion,
                                    foto:data[0].imagen,
                                    unidad:data[0].unidad,
                                    cantidad:data[0].cantidad,
                                    precio:self.precioCarga,
                                });
                        }else{
				console.log("Else linea 130");
                        $http.get('http://'+self.ipServer+':8000/producto/'+barcodeData.text)
                            .success(function(data, status, headers, config) {
                                if(data.length != 0){
                                    //NAVEGO A DAR DE ALTA EL PRODUCTO CON DATOS
                                    $state.go('agregarProducto',
                                            {
                                                ipServer: self.ipServer,
                                                id_supermercados: self.idSuper,
                                                id_producto:barcodeData.text,
                                                precio:self.precioCarga,
                                                nombre:data[0].descripcion,
                                                foto:data[0].imagen,
                                                unidad:data[0].unidad,
                                                cantidad:data[0].cantidad,
                                                existe: true,
                                            });
                                }else{
					console.log("Else linea 147");
                                    //NAVEGO A DAR DE ALTA EL PRODUCTO SIN DATOS
                                    $state.go('agregarProducto',
                                            {
                                                ipServer: self.ipServer,
                                                id_supermercados: self.idSuper,
                                                id_producto:barcodeData.text,
                                                precio:self.precioCarga,
                                                existe: false,
                                            });
                                }
                            });
                        }
                    });
      		}, function(error) {
        	// An error occurred
        		//document.getElementById("tipo").innerHTML="No funciona";
			console.log("Error al escanear!");
      		});
    }
}])

.controller('productoCtrl', ['$scope', '$http',  '$stateParams', 'Config',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $http,$stateParams,Config) {
	console.log("productoCtrl");
    var self = this;
    self.ipServer = $stateParams.ipServer;
    self.idProd= $stateParams.id_producto;
    self.idSuper= $stateParams.id_supermercados;
    self.nombreProd=$stateParams.nombre;
    self.precioProd= $stateParams.precio;
	console.log("Foto cargada: " + $stateParams.foto);
	console.log("Foto filtrada: " + $stateParams.foto);
    self.foto=$stateParams.foto;//.replace(/%2F/g, '/');
    self.unidadProd= $stateParams.unidad;
    self.cantidadProd= $stateParams.cantidad;

    console.log('http://'+self.ipServer+':8000/productosasocsiados/'+self.idSuper+'/'+self.idProd);
    $http.get('http://'+self.ipServer+':8000/productosasocsiados/'+self.idSuper+'/'+self.idProd)
              .success(function(data, status, headers, config) {
              console.log(data);
		document.getElementById('img').setAttribute('src', self.foto);
              var desc ="";
              for(var i in data) {
                  desc = desc + data[i].nombre+data[i].precio+"\n";
              }
              self.comparacionProd = desc;
              });
}])

.controller('agregarProductoCtrl', ['$scope', '$http', '$stateParams', '$cordovaCamera', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope,$http, $stateParams,$cordovaCamera,$state) {
	console.log("agregarProductoCtrl");
    var self = this;
    self.ipServer = $stateParams.ipServer;
    self.idSuper = $stateParams.id_supermercados;
    console.log($stateParams.id_supermercados);
    console.log(self.idSuper);
    self.idNuevoProd = $stateParams.id_producto;
    self.precioNuevoProd = $stateParams.precio;
    self.nombreNuevoProd = $stateParams.nombre;
    self.unidadNuevoProd = $stateParams.unidad;
    self.cantidadNuevoProd = $stateParams.cantidad;
    self.existe = $stateParams.existe;
    self.base64Image = '';
    self.foto = function () {
	console.log("funciotn self.foto");
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 85,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Photo Album
                encodingType: 0,     // 0=JPG 1=PNG
                targetWidth: 500,
                targetHeight: 500
        });

        function onSuccess(imageData) {
		console.log("function onSucess");
            self.base64Image = 'data:image/jpeg;base64,' + imageData;
            self.base64Image = self.base64Image.replace(/\//g, '%2F');
            console.log(self.base64Image);
            document.getElementById('imgPic').setAttribute('src', self.base64Image);
            document.getElementById('foto').value=imageData;
        }

        function onFail(message) {
		alert('Failed because: ' + message);
	    }
    }

    self.submitProducto = function(){
	console.log("function submitProducto");
        if(self.existe){
		console.log("Existe");
            $http.get('http://'+self.ipServer+':8000/producto/prodSuper/'+self.precioNuevoProd+'/'+self.idNuevoProd+'/'+self.idSuper)
            .success(function(data, status, headers, Config) {
                $state.go('producto',{
                                ipServer: self.ipServer,
                                id_supermercados: self.idSuper,
                                id_producto:self.idNuevoProd,
                                nombre:self.nombreNuevoProd,
                                foto:self.base64Image,
                                unidad:self.unidadNuevoProd,
                                cantidad:self.cantidadNuevoProd,
                                precio:self.precioNuevoProd,
                            });
            });
        }else{
		console.log("No existe");
            $http.get('http://'+self.ipServer+':8000/producto/agregar/'+self.idNuevoProd+'/'+self.nombreNuevoProd+'/'+self.base64Image+'/'+self.unidadNuevoProd+'/'+self.cantidadNuevoProd)
            .success(function(data, status, headers, Config) {
                $http.get('http://'+self.ipServer+':8000/producto/prodSuper/'+self.precioNuevoProd+'/'+self.idNuevoProd+'/'+self.idSuper)
                .success(function(data, status, headers, Config) {
                    $state.go('producto',{
                                    ipServer: self.ipServer,
                                    id_supermercados: self.idSuper,
                                    id_producto:self.idNuevoProd,
                                    nombre:self.nombreNuevoProd,
                                    foto:self.base64Image,
                                    unidad:self.unidadNuevoProd,
                                    cantidad:self.cantidadNuevoProd,
                                    precio:self.precioNuevoProd,
                                });
                });
            });
        }
    }
}])
