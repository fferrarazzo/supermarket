angular.module('app.controllers', ['app.config',"ionic.native"])

.controller('superMarketConfigCtrl', ['$scope', '$stateParams','Config','$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,Config,$state) {
    var self = this;
    self.submitIpServer = function(){
                Config.ENV.SERVER = self.ipServer;
                $state.go('superMarket',
                        {
                            server: self.ipServer,
                        })
                }
}])

.controller('superMarketCtrl', ['$scope', '$stateParams', 'Config','$state',  // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, Config,$state) {
    $scope.ENV = $stateParams.ipServer;
    this.selSuperMercado = function(data, status, headers, Config){
                  $state.go('seleccioneSuperMercado',
                          {
                              ipServer: $stateParams.ipServer,
                          })
                  }
    this.addSuperMercado = function(data, status, headers, Config){
                  $state.go('agregarSuperMercado',
                          {
                              ipServer: $stateParams.ipServer,
                          })
                  }
}])

.controller('agregarSuperMercadoCtrl', ['$scope','$http', '$stateParams','Config', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope,$http, $stateParams,Config, $state ) {
    var self = this;
            self.id;
            self.ipServer = $stateParams.ipServer;
    		self.nombreSuper;
    		self.direccionSuper;
    		self.selectedValueLocalidadSuper;
    		self.selectedValueProvinciaSuper;
    $http.get('http://'+Config.ENV.SERVER+':8000/provincias')
            .success(function(data, status, headers, config) {
            self.provinciasList = data;
            });
    $http.get('http://'+Config.ENV.SERVER+':8000/localidades')
            .success(function(data, status, headers, config) {
            self.localidadesList = data;
            });
    $http.get('http://'+Config.ENV.SERVER+':8000/ultimoSuper').success(function(data, status, headers, Config){
            self.id = data[0].idSuper;
        });
    self.submit = function(){
    $http.get('http://'+Config.ENV.SERVER+':8000/supermercados/'+self.id+'/'+self.nombreSuper+'/'+self.direccionSuper+'/'+self.localidadSuperSelectedValue.id_localidad+'/'+self.provinciaSuperSelectedValue.id_provincia)
    .success(function(data, status, headers, Config) {
             		$state.go('superMarket',{ipServer: $stateParams.ipServer,})
                 });
    }
}])

.controller('seleccioneSuperMercadoCtrl', ['$scope', '$http', '$stateParams','Config', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $http, $stateParams,Config, $state ) {
    var self = this;
    self.selectedValue;
    console.log(self.supers);
$http.get('http://'+Config.ENV.SERVER+':8000/supermercados')
        .success(function(data, status, headers, config) {
        console.log(data);
        self.marketList = data;
        });
    self.submit = function(){
        Config.ENV.SUPER_ID=self.selectedValue.id_supermercados;
        Config.ENV.SUPER_LOCALIDAD=self.selectedValue.localidad;
    			$state.go('consulteProducto',
    				{
    				    ipServer: $stateParams.ipServer,
    				    id_supermercados: self.selectedValue.id_supermercados,
    					nombreSuper: self.selectedValue.nombre,
    					localidad: self.selectedValue.localidad,
    				});
    		}
}])

.controller('consulteProductoCtrl', ['$scope', '$stateParams', '$cordovaBarcodeScanner', 'Config', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $cordovaBarcodeScanner, Config, $state) {
	var self = this;
	self.nombreSuper = $stateParams.nombreSuper;
	self.localidad = $stateParams.localidad;
    self.precioCarga;
    console.log(self.nombreSuper);
    console.log(self.localidad);

    this.submitBarCode = function(){
	console.log("Se llamó a la función scope.submitBarCode");
	$cordovaBarcodeScanner
      		.scan()
      		.then(function(barcodeData) {
        	// Success! Barcode data is here
        	//document.getElementById("tipo").innerHTML=barcodeData.format;
        	//document.getElementById("codigo").innerHTML=barcodeData.text;

		// Por ahora asumimos que el producto existe
			console.log("ID del producto = " + barcodeData.text);
			//Existe el Producto?
            $http.get('http://'+Config.ENV.SERVER+':8000/producto/'+barcodeData.text)
                    .success(function(data, status, headers, config) {
                        if(data == null){
                        //NAVEGO A LA PAGINA DE PRODUCTO
                        $state.go('producto',
                                {
                                    ipServer: $stateParams.ipServer,
                                    id_supermercados: $stateParams.id_supermercados,
                                    id_producto:barcodeData.text,
                                });
                        }else{
                        //NAVEGO A DAR DE ALTA EL PRODUCTO
                        $state.go('agregarProducto',
                                {
                                    ipServer: $stateParams.ipServer,
                                    id_supermercados: $stateParams.id_supermercados,
                                    id_producto:barcodeData.text,
                                    precio:self.precioCarga,
                                });
                        }
                    });
      		}, function(error) {
        	// An error occurred
        		//document.getElementById("tipo").innerHTML="No funciona";
			console.log("Error al escanear!");
      		});
    }
}])

.controller('productoCtrl', ['$scope', '$http',  '$stateParams', 'Config',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $http,$stateParams,Config) {
    var self = this;
    self.nombre=$stateParams.nombre;
    //foto:null,
    self.descripcionProd= $stateParams.desc;
    self.unidadProd= $stateParams.unidad;
    self.precioProd= $stateParams.id_producto;
	document.getElementById("producto-input1").innerHTML=$stateParams.id_producto;
    console.log('http://'+$stateParams.ipServe+':8000/productosasocsiados/'+$stateParams.ipServer+'/'+$stateParams.id_producto);
    $http.get('http://'+$stateParams.ipServe+':8000/productosasocsiados/'+$stateParams.ipServer+'/'+$stateParams.id_producto)
              .success(function(data, status, headers, config) {
              console.log(data);
              self.comparacionProd = data;
              });
}])

.controller('agregarProductoCtrl', ['$scope', '$http', '$stateParams', 'Config', '$state',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$cordovaCamera,Config,$state) {
    var self = this;
    self.nombreNuevoProd;
    self.descripcionNuevoProd;
    self.unidadNuevoProd;
    self.submitFoto = function() {
      $cordovaCamera.getPicture({ 	destinationType: Camera.DestinationType.DATA_URL,
                                    encodingType: Camera.EncodingType.JPEG,
                                    correctOrientation:true,
                                    quality:100,
                                    targetWidth:300,

                                    targetHeight:300,
                                    allowEdit:true} ).then(
        (imageData) => {
            console.log("Foto OK");
               //Aca tendriamos que guardar el producto con su foto correspondiente
//               Esto es un ejemplo del profesor no se como funciona
            $http({method:'POST',url:fotoURL+$scope.UPC,data:imageData,headers:{'content-type':'text/plain'}})  },
        (err) => {
            console.log("ERROR Camara: "+err)}
      );
    };
}])
